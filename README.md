Part 1 - Network Prerequisites

The solution includes 5 Python files

Each one of them needs to be placed on either MainServer, Server1, Server2, Server3 or Server4, according to its name

The solution assumes that the servers are discoverable by hostnames mainserver, server1, server2, server3 and server4

Also SSHD needs to run on MainServer on port 22

Instructions for communication check:

* MainServer - rm /tmp/server*
* Server1, Server2, Server3 and Server4 - execute the python files
* MainServer - execute the python file



Part 2 - Build / Deploy Pipeline