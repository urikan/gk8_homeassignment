import paramiko
import socket
def isOpen(ip,port):
   s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
   try:
      s.connect((ip, int(port)))
      s.shutdown(2)
      return True
   except:
      return False

if isOpen("mainserver","3044"):
   result='OK'
else:
   result='Error'

host = "mainserver"
port = 22
username = "mainname"
password = "mainpass"

command = f"echo {result}>/tmp/server1"

ssh = paramiko.SSHClient()
ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
ssh.connect(host, port, username, password)

stdin, stdout, stderr = ssh.exec_command(command)