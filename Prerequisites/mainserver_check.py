import socket,os
def isOpen(ip,port):
   s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
   try:
      s.connect((ip, int(port)))
      s.shutdown(2)
      return True
   except:
      return False


main_report_path='/tmp/connection_report'
if os.path.exists(main_report_path):
   os.remove(main_report_path)
servers=[{'hostname':'server1','port':'3044'},{'hostname':'server2','port':'3044'},{'hostname':'server3','port':'4044'},{'hostname':'server4','port':'4044'}]
for server in servers:
   with open(main_report_path, 'a') as main_report_file:
      if isOpen(server['hostname'],server['port']):
         result='OK'
      else:
         result='Error'
      main_report_file.write(f"MainServer --> {server['hostname'].capitalize()} [{result}]\n")
      server_report_path=f"/tmp/{server['hostname']}"
      if os.path.exists(server_report_path):
         with open(server_report_path, 'r') as server_report_file:
            result=server_report_file.read()
      else:
         result='No results'
      main_report_file.write(f"{server['hostname'].capitalize()} --> MainServer [{result}]\n")